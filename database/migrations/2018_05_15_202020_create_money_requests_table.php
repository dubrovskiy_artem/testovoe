<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoneyRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('money_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('amount');
            $table->integer('type');
            $table->integer('card_id')->unsigned();
            $table->integer('status')->default(\App\MoneyRequest::STATUS_WAITING);
            $table->integer('user_id')->unsigned();
            $table->date('date');
            $table->timestamps();

            $table->foreign('card_id')->references('id')->on('cards');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card_replenishment_requests');
    }
}
