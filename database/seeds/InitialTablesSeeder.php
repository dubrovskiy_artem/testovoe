<?php

use App\Card;
use App\Currency;
use App\MoneyRequest;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Crypt;

class InitialTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Currency::create([
            'name' => 'USD'
        ]);

        User::create([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('q1w2e3r4t5'),
            'is_admin' => \App\User::ROLE_ADMIN
        ]);
    }
}
