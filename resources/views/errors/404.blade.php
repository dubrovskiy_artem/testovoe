@extends('layouts.clear')

@section('content')
    <div class="container">
        <div class="card card-login mx-auto mt-5">
            <div class="card-header">404</div>
            <div class="card-body">
                <h1>404</h1>
                <p>Page not found</p>
            </div>
        </div>
    </div>
@endsection
