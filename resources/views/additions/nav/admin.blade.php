<ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Пользователи">
        <a class="nav-link" href="{{ url('admin/users') }}">
            <span class="nav-link-text">Пользователи</span>
        </a>
    </li>
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Карты">
        <a class="nav-link" href="{{ url('admin/cards') }}">
            <span class="nav-link-text">Карты</span>
        </a>
    </li>
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Заявки">
        <a class="nav-link" href="{{ url('admin/money-requests') }}">
            <span class="nav-link-text">Заявки</span>
        </a>
    </li>
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Статистика">
        <a class="nav-link" href="{{ url('admin/statistics') }}">
            <span class="nav-link-text">Статистика</span>
        </a>
    </li>
</ul>