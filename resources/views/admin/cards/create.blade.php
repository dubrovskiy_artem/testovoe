@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card card-login mx-auto mt-5">
            <div class="card-header">Добавление карты</div>
            <div class="card-body">
                <form class="form-horizontal" method="POST" action="{{ url('admin/cards') }}">
                    {{ csrf_field() }}
                    <div class="form-group {{ $errors->has('amount') ? ' has-error' : '' }}">
                        <label for="number">Номер</label>
                        <input data-mask="9999999999999999" id="number" class="form-control" name="number" type="text" value="{{ old('number') }}" required autofocus placeholder="Enter number">
                        @if ($errors->has('number'))
                            <span class="help-block">
                                <strong>{{ $errors->first('number') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('cvv2') ? ' has-error' : '' }}">
                        <label for="cvv2">CVV2</label>
                        <input data-mask="999" id="cvv2" class="form-control" name="cvv2" type="text" value="{{ old('cvv2') }}" required autofocus placeholder="Enter cvv2">
                        @if ($errors->has('cvv2'))
                            <span class="help-block">
                                <strong>{{ $errors->first('cvv2') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('expiry_date') ? ' has-error' : '' }}">
                        <label for="expiry_date">Дата</label>
                        <input data-mask="99/99" id="expiry_date" class="form-control" name="expiry_date" type="text" value="{{ old('expiry_date') }}" required autofocus placeholder="Enter date">
                        @if ($errors->has('expiry_date'))
                            <span class="help-block">
                                <strong>{{ $errors->first('expiry_date') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('balance') ? ' has-error' : '' }}">
                        <label for="balance">Баланс</label>
                        <input id="balance" class="form-control" name="balance" type="number" min="0" value="{{ old('balance') }}" required autofocus placeholder="Enter balance">
                        @if ($errors->has('balance'))
                            <span class="help-block">
                                <strong>{{ $errors->first('balance') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('currency') ? ' has-error' : '' }}">
                        <label for="currency">Валюта</label>
                        <select name="currency" class="form-control" id="currency">
                            @foreach($currencies as $currency)
                                <option value="{{ $currency->id }}">{{ $currency->name }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('currency'))
                            <span class="help-block">
                                <strong>{{ $errors->first('currency') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('user') ? ' has-error' : '' }}">
                        <label for="user">Пользователь</label>
                        <select name="user" class="form-control" id="user">
                            <option value=""></option>
                            @foreach($users as $user)
                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('user'))
                            <span class="help-block">
                                <strong>{{ $errors->first('user') }}</strong>
                            </span>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Добавить карту</button>
                </form>
            </div>
        </div>
    </div>
@endsection
