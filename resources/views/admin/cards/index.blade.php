@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="card mb-3">
            <div class="card-header">
                <i class="fa fa-table"></i> Карты
                <a class="btn btn-outline-success btn-sm pull-right" href="{{ url('admin/cards/create') }}">Добавить</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Код</th>
                            <th>CVV2</th>
                            <th>Дата</th>
                            <th>Баланс</th>
                            <th>Валюта</th>
                            <th>Пользователь</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Код</th>
                            <th>CVV2</th>
                            <th>Дата</th>
                            <th>Баланс</th>
                            <th>Валюта</th>
                            <th>Пользователь</th>
                            <th>Действия</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($cards as $card)
                            <tr>
                                <td>{{ $card->number }}</td>
                                <td>{{ $card->cvv2 }}</td>
                                <td>{{ $card->expiry_date }}</td>
                                <td>{{ $card->balance }}</td>
                                <td>{{ $card->currency->name }}</td>
                                <td>{{ $card->user->name ?? '' }}</td>
                                <td>
                                    <form method="post" action="cards/{{ $card->id }}">
                                        {{ method_field('DELETE') }}
                                        <div class="form-group">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-outline-danger btn-sm">Удалить</button>
                                            </div>
                                        </div>
                                    </form>
                                    <a href="{{ url('admin/cards/' . $card->id . '/edit') }}" class="btn btn-outline-primary btn-sm">Редактировать</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
