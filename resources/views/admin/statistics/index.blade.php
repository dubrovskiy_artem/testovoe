@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="card mb-3">
            <div class="card-header">
                <i class="fa fa-table"></i> Статистика</div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Подтвержденные пополнения</th>
                            <th>Подтвержденные списания</th>
                            <th>Дата</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Подтвержденные пополнения</th>
                            <th>Подтвержденные списания</th>
                            <th>Дата</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($statistics as $statistic)
                            <tr>
                                <td>{{ $statistic->replenishment }}</td>
                                <td>{{ $statistic->writeOff }}</td>
                                <td>{{ $statistic->date }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
