@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="card mb-3">
            <div class="card-header">
                <i class="fa fa-table"></i> Запросы</div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Карта</th>
                            <th>Пользователь</th>
                            <th>Обьем</th>
                            <th>Тип</th>
                            <th>Статус</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Карта</th>
                            <th>Пользователь</th>
                            <th>Обьем</th>
                            <th>Тип запроса</th>
                            <th>Статус</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($moneyRequests as $moneyRequest)
                            <tr>
                                <td>{{ $moneyRequest->card->number }}</td>
                                <td>{{ $moneyRequest->user->name }}</td>
                                <td>{{ $moneyRequest->amount }}</td>
                                <td>{{ \App\MoneyRequest::TYPE_LABELS[$moneyRequest->type] }}</td>
                                <td>{{ \App\MoneyRequest::STATUS_LABELS[$moneyRequest->status] }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
