@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="card mb-3">
            <div class="card-header">
                <i class="fa fa-table"></i> Карты</div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Код</th>
                            <th>CVV2</th>
                            <th>Дата</th>
                            <th>Баланс</th>
                            <th>Валюта</th>
                            <th>Пользователь</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Код</th>
                            <th>CVV2</th>
                            <th>Дата</th>
                            <th>Баланс</th>
                            <th>Валюта</th>
                            <th>Пользователь</th>
                            <th>Действия</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($cards as $card)
                            <tr>
                                <td>{{ $card->number }}</td>
                                <td>{{ $card->cvv2 }}</td>
                                <td>{{ $card->expiry_date }}</td>
                                <td>{{ $card->balance }}</td>
                                <td>{{ $card->currency->name }}</td>
                                <td>{{ $card->user->name ?? '' }}</td>
                                <td>
                                    <a class="btn btn-sm btn-outline-success" href="{{ url('money-requests/' . $card->id . '/create') }}">Создать заявку</a>
                                    <a class="btn btn-sm btn-outline-danger" href="{{ url('cards/' . $card->id . '/unbind') }}">Отвязать</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
