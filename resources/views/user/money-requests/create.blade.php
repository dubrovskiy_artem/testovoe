@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card card-login mx-auto mt-5">
            <div class="card-header">Создание заявки</div>
            <div class="card-body">
                <form class="form-horizontal" method="POST" action="{{ url('money-requests/' . $card->id . '/store') }}">
                    {{ csrf_field() }}
                    <div class="form-group {{ $errors->has('amount') ? ' has-error' : '' }}">
                        <label for="amount">Сумма</label>
                        <input id="amount" class="form-control" name="amount" type="number" min="0" value="{{ old('amount') }}" required autofocus placeholder="Enter amount">
                        @if ($errors->has('amount'))
                            <span class="help-block">
                                <strong>{{ $errors->first('amount') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('type') ? ' has-error' : '' }}">
                        <label for="type">Тип</label>
                        <select name="type" class="form-control" id="type">
                            @foreach(\App\MoneyRequest::TYPE_LABELS as $key => $type)
                                <option value="{{ $key }}">{{ $type }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('type'))
                            <span class="help-block">
                                <strong>{{ $errors->first('type') }}</strong>
                            </span>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Подать заявку</button>
                </form>
            </div>
        </div>
    </div>
@endsection
