<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Crypt;

/**
 * Class Card
 * @package App
 * @property string number
 * @property string cvv2
 * @property string expiry_date
 * @property int balance
 * @property int currency_id
 * @property int user_id
 */
class Card extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'number',
        'cvv2',
        'expiry_date',
        'balance',
        'currency_id',
        'user_id',
    ];

    public function getNumberAttribute($value)
    {
        return Crypt::decryptString($value);
    }

    public function getCvv2Attribute($value)
    {
        return Crypt::decryptString($value);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function moneyRequests()
    {
        return $this->hasMany(MoneyRequest::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function waitMoneyRequests()
    {
        return $this->hasMany(MoneyRequest::class)->where('status', MoneyRequest::STATUS_WAITING);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function userUnbind()
    {
        $this->user_id = null;
        $this->save();
    }
}
