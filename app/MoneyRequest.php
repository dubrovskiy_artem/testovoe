<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MoneyRequest extends Model
{
    protected $fillable = [
        'amount',
        'type',
        'card_id',
        'status',
        'user_id',
        'date',
    ];

    const STATUS_WAITING = 1;
    const STATUS_REJECT = 2;
    const STATUS_APPROVE = 3;

    const STATUS_LABELS = [
        self::STATUS_WAITING => 'Ожидание',
        self::STATUS_REJECT => 'Отказ',
        self::STATUS_APPROVE => 'Подтвержден',
    ];

    const TYPE_REPLENISHMENT = 1;
    const TYPE_WRITE_OFF = 2;

    const TYPE_LABELS = [
        self::TYPE_REPLENISHMENT => 'Пополнение',
        self::TYPE_WRITE_OFF => 'Списание'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function card()
    {
        return $this->belongsTo(Card::class);
    }

    public function approveAmount()
    {

    }
}
