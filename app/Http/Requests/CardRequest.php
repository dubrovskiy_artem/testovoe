<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number' => 'required|digits:16',
            'cvv2' => ['required', 'regex:/^[0-9]{3,4}$/'],
            'expiry_date' => ['required', 'regex:/\b(0[1-9]|1[0-2])\/?([0-9]{2}|[0-9]{2})\b/'],
            'balance' => 'nullable|numeric',
            'currency' => 'required|integer|exists:currencies,id',
            'user' => 'nullable|integer|exists:users,id',
        ];
    }
}
