<?php

namespace App\Http\Controllers;

use App\MoneyRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class StatisticsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subQuery = MoneyRequest::query();
        $subQuery->where('status', MoneyRequest::STATUS_APPROVE);
        $subQuery->where('user_id', Auth::id());

        $statistics = DB::table( DB::raw("({$subQuery->toSql()}) as sub") )
            ->mergeBindings($subQuery->getQuery())
            ->select(
                'sub.date',
                DB::raw('( SELECT sum( amount ) FROM money_requests WHERE date = sub.date AND `type` = ? ) AS replenishment'),
                DB::raw('( SELECT sum( amount ) FROM money_requests WHERE date = sub.date AND `type` = ? ) AS writeOff')
            )
            ->addBinding([
                MoneyRequest::TYPE_REPLENISHMENT,
                MoneyRequest::TYPE_WRITE_OFF
            ], 'select')
            ->groupBy('sub.date')
            ->orderBy('sub.date', 'desc')
            ->get();

        return view('user.statistics.index', compact('statistics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
