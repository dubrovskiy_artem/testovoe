<?php

namespace App\Http\Controllers;

use App\Card;
use App\MoneyRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class MoneyRequestsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $moneyRequests = MoneyRequest::where('user_id', Auth::id())->with(['card' => function ($query){
            $query->withTrashed();
        }])->get();

        return view('user.money-requests.index', compact('moneyRequests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $card = Card::findOrFail($id);
        return view('user.money-requests.create', compact('card'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $card = Card::whereId($id)->whereUserId(Auth::id())->firstOrFail();

        $this->validate($request, [
            'amount' => 'required',
            'type' => 'required'
        ]);

        $moneyRequest = new MoneyRequest();
        $moneyRequest->amount = $request->amount;
        $moneyRequest->type = $request->type;
        $moneyRequest->card_id = $card->id;
        $moneyRequest->user_id = Auth::id();
        $moneyRequest->date = (Carbon::now())->format('Y-m-d');
        $moneyRequest->save();

        return redirect('money-requests')->with('success', 'Money request submitted successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
