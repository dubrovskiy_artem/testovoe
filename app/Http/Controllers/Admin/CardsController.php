<?php

namespace App\Http\Controllers\Admin;

use App\Card;
use App\Currency;
use App\Http\Requests\CardRequest;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;

class CardsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cards = Card::all();
        return view('admin.cards.index', compact('cards'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $currencies = Currency::all();
        $users = User::where('is_admin', 0)->get();

        return view('admin.cards.create', compact('currencies', 'users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CardRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CardRequest $request)
    {
        $card = new Card();
        $card->number = Crypt::encryptString($request->number);
        $card->cvv2 = Crypt::encryptString($request->cvv2);
        $card->expiry_date = $request->expiry_date;
        $card->balance = $request->balance;
        $card->currency_id = $request->currency;
        $card->user_id = $request->user;
        $card->save();

        return redirect('admin/cards')->with('success', 'Card added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $card = Card::findOrFail($id);
        $currencies = Currency::all();
        $users = User::where('is_admin', 0)->get();

        return view('admin.cards.edit', compact('card', 'currencies', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CardRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CardRequest $request, $id)
    {
        $card = Card::findOrFail($id);
//        $card->number = $request->number;
//        $card->cvv2 = $request->cvv2;
//        $card->expiry_date = $request->expiry_date;
//        $card->balance = $request->balance;
//        $card->currency_id = $request->currency;
        $card->user_id = $request->user;
        $card->save();

        return redirect()->back()->with('success', 'Card update successful');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $card = Card::where('id', $id)->where('balance', 0)->firstOrFail();

        $card->delete();

        return redirect('');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function historyReplenishmentRequests($id)
    {
        $card = Card::findOrFail($id);

        return view('', compact('card'));
    }
}
