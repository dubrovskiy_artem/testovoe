<?php

namespace App\Http\Controllers\Admin;

use App\Card;
use App\MoneyRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MoneyRequestsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $moneyRequests = MoneyRequest::with(['card' => function ($query){
            $query->withTrashed();
        }])->get();

        return view('admin.money-requests.index', compact('moneyRequests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function approve($id)
    {
        $replenishmentRequest = MoneyRequest::findOrFail($id);
        $replenishmentRequest->status = MoneyRequest::STATUS_APPROVE;

        if(
            $replenishmentRequest->status === MoneyRequest::TYPE_WRITE_OFF &&
            $replenishmentRequest->card->balance < $replenishmentRequest->amount
        ){
            return redirect()->back()->with('error', 'The balance on the card is less than the requested amount');
        }

        if($replenishmentRequest->status === MoneyRequest::TYPE_REPLENISHMENT){
            $replenishmentRequest->card->increment('balance', $replenishmentRequest->amount);
        }else{
            $replenishmentRequest->card->decrement('balance', $replenishmentRequest->amount);
        }

        $replenishmentRequest->push();

        return redirect('')->with('success', 'The request was confirmed successfully');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function reject($id)
    {
        $replenishmentRequest = MoneyRequest::findOrFail($id);
        $replenishmentRequest->status = MoneyRequest::STATUS_REJECT;
        $replenishmentRequest->save();

        return redirect('')->with('success', 'The request was declined successfully');
    }
}
