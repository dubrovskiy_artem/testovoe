<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::group(['middleware' => ['auth']], function () {

    Route::get('/', function () {
        if(\Illuminate\Support\Facades\Auth::user()->isAdmin()){
            return redirect('admin/cards');
        }else{
            return redirect('cards');
        }
    });
    Route::group(['middleware' => ['auth', 'admin'], 'namespace' => 'Admin', 'prefix' => 'admin'], function () {
        Route::get('users/{id}/set-admin', 'UsersController@setAdmin');
        Route::resource('users', 'UsersController');
        Route::resource('cards', 'CardsController');
        Route::resource('money-requests', 'MoneyRequestsController');
        Route::resource('statistics', 'StatisticsController');
    });
    Route::resource('cards', 'CardsController');
    Route::get('cards/{id}/unbind', 'CardsController@unbind');
    Route::resource('statistics', 'StatisticsController');
    Route::get('money-requests', 'MoneyRequestsController@index');
    Route::get('money-requests/{id}/create', 'MoneyRequestsController@create');
    Route::post('money-requests/{id}/store', 'MoneyRequestsController@store');
});

Route::get('/home', 'HomeController@index')->name('home');
